<?php

namespace Smorken\Squeue\Tests\Feature\Commands;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery as m;
use Smorken\Squeue\Models\Eloquent\Squeue;
use Smorken\Squeue\Tests\TestCase;
use Smorken\Squeue\Tests\Unit\Handlers\BarModel;
use Smorken\Squeue\Tests\Unit\Handlers\FooModel;

class ProcessorTest extends TestCase
{

    use RefreshDatabase;

    public function testProcessorAllProcessesMixed(): void
    {
        $s = [
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'foo',
                'data' => [FooModel::class => 'abc', 'email' => 'foo@example.edu'],
            ]),
            Squeue::factory()->create([
                'handler_id' => 'squeue_noaction', 'identity' => 'bar', 'data' => [BarModel::class => 'abc'],
            ]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:process',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('complete_ok: 2', $out);
        $this->assertStringContainsString('complete_error: 0', $out);
        $this->assertStringContainsString('error: 0', $out);
    }

    public function testProcessorEmailerResultsAndError(): void
    {
        $s = [
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'foo',
                'data' => [FooModel::class => 'abc', 'email' => 'foo@example.edu'],
            ]),
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'bar', 'data' => [BarModel::class => 'abc'],
            ]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:process',
                    'handler_id' => 'squeue_emailer',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('complete_ok: 1', $out);
        $this->assertStringContainsString('complete_error: 0', $out);
        $this->assertStringContainsString('error: 1', $out);
    }

    public function testProcessorMixedOnlyProcessesHandlerId(): void
    {
        $s = [
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'foo',
                'data' => [FooModel::class => 'abc', 'email' => 'foo@example.edu'],
            ]),
            Squeue::factory()->create([
                'handler_id' => 'squeue_noaction', 'identity' => 'bar', 'data' => [BarModel::class => 'abc'],
            ]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:process',
                    'handler_id' => 'squeue_emailer',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('complete_ok: 1', $out);
        $this->assertStringContainsString('complete_error: 0', $out);
        $this->assertStringContainsString('error: 0', $out);
    }

    public function testProcessorNoActionResults(): void
    {
        $s = [
            Squeue::factory()->create(['identity' => 'foo', 'data' => [FooModel::class => 'abc']]),
            Squeue::factory()->create(['identity' => 'bar', 'data' => [BarModel::class => 'abc']]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:process',
                    'handler_id' => 'squeue_noaction',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('complete_ok: 2', $out);
        $this->assertStringContainsString('complete_error: 0', $out);
        $this->assertStringContainsString('error: 0', $out);
    }

    public function testProcessorNoResults(): void
    {
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:process',
                    'handler_id' => 'squeue_noaction',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('complete_ok: 0', $out);
        $this->assertStringContainsString('complete_error: 0', $out);
        $this->assertStringContainsString('error: 0', $out);
    }

    protected function getMocks(): array
    {
        $e = m::mock(Dispatcher::class);
        $e->shouldReceive('fire');
        $e->shouldReceive('dispatch');
        $kernel = new Kernel($this->app, $e);
        return [$kernel, $e];
    }
}
