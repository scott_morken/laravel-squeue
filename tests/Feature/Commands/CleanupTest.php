<?php

namespace Smorken\Squeue\Tests\Feature\Commands;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Foundation\Console\Kernel;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery as m;
use Smorken\Squeue\Models\Eloquent\Squeue;
use Smorken\Squeue\Tests\TestCase;
use Smorken\Squeue\Tests\Unit\Handlers\BarModel;
use Smorken\Squeue\Tests\Unit\Handlers\FooModel;

class CleanupTest extends TestCase
{

    use RefreshDatabase;

    public function testCleanupNotProcessedIsNoResults(): void
    {
        $s = [
            Squeue::factory()->create([
                'identity' => 'foo', 'data' => [FooModel::class => 'abc'],
                'created_at' => date('Y-m-d H:i:s', strtotime('-2 months')),
            ]),
            Squeue::factory()->create(['identity' => 'bar', 'data' => [BarModel::class => 'abc']]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:cleanup',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('Processed 0 records.', $out);
    }

    public function testCleanupRecordsInsideRange(): void
    {
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:cleanup',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('Processed 0 records.', $out);
    }

    public function testCleanupWithCompletedIsResult(): void
    {
        $s = [
            Squeue::factory()->create([
                'identity' => 'foo', 'data' => [FooModel::class => 'abc'],
                'created_at' => date('Y-m-d H:i:s', strtotime('-2 months')),
                'completed_at' => date('Y-m-d H:i:s', strtotime('-1 week')),
            ]),
            Squeue::factory()->create(['identity' => 'bar', 'data' => [BarModel::class => 'abc']]),
        ];
        [$kernel, $e] = $this->getMocks();
        $status = $kernel->handle(
            $input = new \Symfony\Component\Console\Input\ArrayInput(
                [
                    'command' => 'squeue:cleanup',
                ]
            ),
            $output = new \Symfony\Component\Console\Output\BufferedOutput
        );
        $out = trim($output->fetch());
        $this->assertStringContainsString('Processed 1 records.', $out);
    }

    protected function getMocks(): array
    {
        $e = m::mock(Dispatcher::class);
        $e->shouldReceive('fire');
        $e->shouldReceive('dispatch');
        $kernel = new Kernel($this->app, $e);
        return [$kernel, $e];
    }
}
