<?php

namespace Smorken\Squeue\Tests\Feature\Controller;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Smorken\Squeue\Models\Eloquent\Squeue;
use Smorken\Squeue\Tests\TestCase;
use Smorken\Squeue\Tests\Unit\Handlers\BarModel;
use Smorken\Squeue\Tests\Unit\Handlers\FooModel;

class SqueueControllerTest extends TestCase
{

    use RefreshDatabase;

    public function testBaseRoute()
    {
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->see('Simple Queue Administration')
             ->see('No records found.');
    }

    public function testBaseRouteWithRecords()
    {
        $s = Squeue::factory()->create();
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->see('Simple Queue Administration')
             ->see($s->identity)
             ->dontSee('No records found.');
    }

    public function testProcessRecordsWithEmailer()
    {
        $s = [
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'foo',
                'data' => [FooModel::class => 'abc', 'email' => 'foo@example.edu'],
            ]),
            Squeue::factory()->create([
                'handler_id' => 'squeue_emailer', 'identity' => 'bar', 'data' => [BarModel::class => 'abc'],
            ]),
        ];
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->see('Simple Queue Administration')
             ->see('<td>P</td>')
             ->see('<td>0</td>')
             ->click('squeue_emailer')
             ->seePageIs('/admin/squeue')
             ->see('<td>C</td>')
             ->see('<td>1</td>')
             ->see('<td>E</td>')
             ->dontSee('<td>P</td>')
             ->dontSee('<td>0</td>');
    }

    public function testProcessRecordsWithNoAction()
    {
        $s = [
            Squeue::factory()->create(['identity' => 'foo', 'data' => [FooModel::class => 'abc']]),
            Squeue::factory()->create(['identity' => 'bar', 'data' => [BarModel::class => 'abc']]),
        ];
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->see('Simple Queue Administration')
             ->see('<td>P</td>')
             ->see('<td>0</td>')
             ->click('squeue_noaction')
             ->seePageIs('/admin/squeue')
             ->see('<td>C</td>')
             ->see('<td>1</td>')
             ->dontSee('<td>P</td>')
             ->dontSee('<td>0</td>');
    }

    public function testViewRecordNoData()
    {
        $s = Squeue::factory()->create();
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->click($s->id)
             ->seePageIs('/admin/squeue/view/'.$s->id)
             ->see($s->identity)
             ->see('Queue lookup results: '.$s->id);
    }

    public function testViewRecordNoIdMethod()
    {
        $s = Squeue::factory()->create(['data' => [BarModel::class => 'abc']]);
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->click($s->id)
             ->seePageIs('/admin/squeue/view/'.$s->id)
             ->see($s->identity)
             ->see('Queue lookup results: '.$s->id)
             ->see('<div><b>Smorken\Squeue\Tests\Unit\Handlers\BarModel:</b> abc');
    }

    public function testViewRecordWithModelViaFind()
    {
        $s = Squeue::factory()->create(['data' => [FooModel::class => 'abc']]);
        $this->actingAs($this->authUser())
             ->visit('/admin/squeue')
             ->click($s->id)
             ->seePageIs('/admin/squeue/view/'.$s->id)
             ->see($s->identity)
             ->see('Queue lookup results: '.$s->id)
             ->see('<div><b>Smorken\Squeue\Tests\Unit\Handlers\FooModel:</b> Mockery_');
    }
}
