<?php

namespace Smorken\Squeue\Tests\Unit;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Collection;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Squeue\Contracts\Handlers\Base;
use Smorken\Squeue\Contracts\Storage\Handler;
use Smorken\Squeue\Processor;
use Smorken\Squeue\Results;

class ProcessorTest extends TestCase
{

    public function testGetHandlerByIdIsNullWithNoMatch(): void
    {
        [$sut, $a, $h] = $this->getSut();
        $h->shouldReceive('find')->with('foo')->once()->andReturn(null);
        $this->assertNull($sut->getHandlerById('foo'));
    }

    public function testGetHandlerByIdIsObject(): void
    {
        [$sut, $a, $h] = $this->getSut();
        $handler = $this->mockHandler();
        $h->shouldReceive('find')->with('foo')->once()->andReturn($this->mockHandlerModel('foo', 'FooClass'));
        $a->shouldReceive('make')->with('InterfaceFooClass')->once()->andReturn($handler);
        $this->assertEquals($handler, $sut->getHandlerById('foo'));
    }

    public function testProcessAll(): void
    {
        [$sut, $a, $h] = $this->getSut();
        $hm1 = $this->mockHandlerModel('foo', 'FooClass');
        $hm2 = $this->mockHandlerModel('bar', 'BarClass');
        $handler1 = $this->mockHandler();
        $handler2 = $this->mockHandler();
        $h->shouldReceive('all')->once()->andReturn(new Collection([$hm1, $hm2]));
        $a->shouldReceive('make')->with('InterfaceFooClass')->once()->andReturn($handler1);
        $a->shouldReceive('make')->with('InterfaceBarClass')->once()->andReturn($handler2);
        $handler1->shouldReceive('handle')
                 ->with('foo', [])
                 ->andReturn((new Results())->increment(\Smorken\Squeue\Contracts\Results::COMPLETE_OK, 3)
                                            ->increment(\Smorken\Squeue\Contracts\Results::ERROR, 1));
        $handler2->shouldReceive('handle')
                 ->with('bar', [])
                 ->andReturn((new Results())->increment(\Smorken\Squeue\Contracts\Results::COMPLETE_OK, 2)
                                            ->increment(\Smorken\Squeue\Contracts\Results::ERROR, 5));
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 6,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 5,
        ];
        $this->assertEquals($expected, $sut->processAll()->toArray());
    }

    public function testProcessWithHandler(): void
    {
        $results = new Results();
        [$sut, $a, $h] = $this->getSut();
        $handler = $this->mockHandler();
        $h->shouldReceive('find')->with('foo')->once()->andReturn($this->mockHandlerModel('foo', 'FooClass'));
        $a->shouldReceive('make')->with('InterfaceFooClass')->once()->andReturn($handler);
        $handler->shouldReceive('handle')->with('foo', [])->andReturn($results);
        $this->assertEquals($results, $sut->process('foo'));
    }

    public function testProcessWithNullHandler(): void
    {
        [$sut, $a, $h] = $this->getSut();
        $h->shouldReceive('find')->with('foo')->once()->andReturn(null);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $sut->process('foo')->toArray());
    }

    protected function getSut(): array
    {
        $a = m::mock(Application::class);
        $h = m::mock(Handler::class);
        $sut = new Processor($a, $h);
        return [$sut, $a, $h];
    }

    protected function mockHandler(): Base
    {
        return m::mock(Base::class);
    }

    protected function mockHandlerModel(string $id, string $klass): \Smorken\Squeue\Contracts\Models\Handler
    {
        $m = m::mock(\Smorken\Squeue\Contracts\Models\Handler::class);
        $m->id = $id;
        $m->interface = 'Interface'.$klass;
        $m->handler = $klass;
        return $m;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
