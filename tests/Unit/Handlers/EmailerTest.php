<?php

namespace Smorken\Squeue\Tests\Unit\Handlers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Squeue\Contracts\Storage\Squeue;
use Smorken\Squeue\Handlers\Emailer;

class EmailerTest extends Base
{

    public function testHandleNoPendingModels(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(new Collection());
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModel(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$this->getSqueue()])
        );
        $m->shouldReceive('send')->with('foo.view', m::type('array'), m::type('closure'))->andReturn(true);
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 1,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndException(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$this->getSqueue()])
        );
        $m->shouldReceive('send')->with('foo.view', m::type('array'), m::type('closure'))
          ->andThrow(new \Exception('foo'));
        $l->shouldReceive('report');
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndLookups(): void
    {
        [$sut, $m, $l, $p, $a] = $this->getSut($this->getProviders());
        $sm = $this->getSqueue();
        $sm->data = [
            FooModel::class => 1,
        ];
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$sm])
        );
        $foomodel = new FooModel();
        $m->shouldReceive('send')->withArgs(
            function ($view, $data, $closure) {
                $group = $data['lookups']->first();
                $m = $group[FooModel::class];
                return $view === 'smorken/squeue::emailer.default' &&
                    count($data['lookups']) === 1 &&
                    $m instanceof FooModel &&
                    is_callable($closure);
            }
        )->andReturn(true);
        $a->shouldReceive('make')->with(FooModel::class)->andReturn($foomodel);
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 1,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndSendFailure(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$this->getSqueue()])
        );
        $e = new \Exception();
        $m->shouldReceive('send')->with('foo.view', m::type('array'), m::type('closure'))->andThrow($e);
        $l->shouldReceive('report')->once()->with($e);
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModels(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $coll = new Collection();
        $coll->push($this->getSqueue(1));
        $coll->push($this->getSqueue(1));
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            $coll
        );
        $m->shouldReceive('send')->with('foo.view', m::type('array'), m::type('closure'))->andReturn(true);
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 2,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelsAndExceptionAndMaxAttempts(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $coll = new Collection();
        $sm = $this->getSqueue(1);
        $sm->attempts = 3;
        $coll->push($sm);
        $coll->push($this->getSqueue(1));
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            $coll
        );
        $l->shouldReceive('report')->once();
        $m->shouldReceive('send')
          ->with('foo.view', m::type('array'), m::type('closure'))
          ->andThrow(new \Exception('foo'));
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithMultipleIdentitiesAndModelsAndExceptionAndMaxAttempts(): void
    {
        [$sut, $m, $l, $p] = $this->getSut($this->getProviders());
        $coll = new Collection();
        $sm = $this->getSqueue(1);
        $sm->attempts = 3;
        $coll->push($sm);
        $coll->push($this->getSqueue(1));
        $coll->push($this->getSqueue(2));
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            $coll
        );
        $l->shouldReceive('report')->once();//->andReturnUsing(function ($e) { echo $e->getMessage(); });
        $m->shouldReceive('send')->once()->andThrow(new \Exception('foo'));
        $m->shouldReceive('send')->once()->andReturn(true);
        $results = $sut->handle('squeue.foo', ['view_name' => 'foo.view']);
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 1,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    protected function getSut(array $providers = []): array
    {
        $a = m::mock(Application::class);
        $l = m::mock(ExceptionHandler::class);
        $p = m::mock(Squeue::class);
        $m = m::mock(Mailer::class);
        $sut = new Emailer($m, $a, $l, $p);
        return [$sut, $m, $l, $p, $a];
    }
}
