<?php

namespace Smorken\Squeue\Tests\Unit\Handlers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Collection;
use Mockery as m;
use Smorken\Squeue\Contracts\Storage\Squeue;
use Smorken\Squeue\Handlers\NoAction;

class NoActionTest extends Base
{
    public function testGetProviderWithNoProviderIsNull(): void
    {
        [$sut, $l, $p, $a] = $this->getSut();
        $e = new \ReflectionException('No Foo!');
        $a->shouldReceive('make')->with('foo')->andThrow($e);
        $l->shouldReceive('report')->with($e);
        $this->assertNull($sut->getProvider('foo'));
    }

    public function testHandleNoPendingModels(): void
    {
        [$sut, $l, $p] = $this->getSut(false, $this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(new Collection());
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModel(): void
    {
        [$sut, $l, $p] = $this->getSut(false, $this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$this->getSqueue()])
        );
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 1,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndError(): void
    {
        [$sut, $l, $p] = $this->getSut(true, $this->getProviders());
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$this->getSqueue()])
        );
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndErrorComplete(): void
    {
        [$sut, $l, $p] = $this->getSut(true, $this->getProviders());
        $s = $this->getSqueue();
        $s->attempts = 3;
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$s])
        );
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 1,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 0,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    public function testHandleWithModelAndLookups(): void
    {
        [$sut, $l, $p, $a] = $this->getSut(false, $this->getProviders());
        $sm = $this->getSqueue();
        $sm->data = [
            'FooClass' => 1,
        ];
        $p->shouldReceive('getPendingByHandler')->once()->with('squeue.foo')->andReturn(
            new Collection([$sm])
        );
        $foomodel = new FooModel();
        $a->shouldReceive('make')->with('FooClass')->andReturn($foomodel);
        $results = $sut->handle('squeue.foo');
        $expected = [
            \Smorken\Squeue\Contracts\Results::ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_ERROR => 0,
            \Smorken\Squeue\Contracts\Results::COMPLETE_OK => 1,
        ];
        $this->assertEquals($expected, $results->toArray());
    }

    protected function getSut(bool $err_class = false, array $providers = []): array
    {
        $a = m::mock(Application::class);
        $l = m::mock(ExceptionHandler::class);
        $p = m::mock(Squeue::class);
        if ($err_class) {
            $sut = new NoActionError($a, $l, $p);
        } else {
            $sut = new NoAction($a, $l, $p);
        }
        if ($providers) {
            $sut->setProviders($providers);
        }
        return [$sut, $l, $p, $a];
    }
}
