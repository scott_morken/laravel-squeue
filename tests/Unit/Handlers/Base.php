<?php


namespace Smorken\Squeue\Tests\Unit\Handlers;


use Illuminate\Database\ConnectionResolver;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class Base extends TestCase
{

    public function setUp(): void
    {
        parent::setUp();
        date_default_timezone_set('UTC');
    }

    public function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }

    protected function getProviders()
    {
        return [
            'foo' => m::mock('FooProvider'),
        ];
    }

    protected function getSqueue($identity = 0, $handler_id = 'squeue.foo', $data = [])
    {
        \Smorken\Squeue\Models\Eloquent\Squeue::setConnectionResolver($this->mockConnectionResolver());
        $m = m::mock(new \Smorken\Squeue\Models\Eloquent\Squeue());
        $m->identity = $identity;
        $m->handler_id = $handler_id;
        $m->data = $data;
        $m->attempts = 0;
        $m->existing = true;
        return $m;
    }

    protected function mockConnectionResolver()
    {
        $ci = m::mock('Illuminate\Database\ConnectionInterface');
        $g = m::mock('Illuminate\Database\Query\Grammars\Grammar');
        $p = m::mock('Illuminate\Database\Query\Processors\Processor');
        $ci->shouldReceive('getQueryGrammar')->andReturn($g);
        $ci->shouldReceive('getName')->andReturn('default');
        $g->shouldReceive('getDateFormat')->andReturn('Y-m-d H:i:s');
        $ci->shouldReceive('getPostProcessor')->andReturn($p);
        $g->shouldReceive('compileInsertGetId')->andReturn('compile insert get id');
        $p->shouldReceive('processInsertGetId')->andReturn('process insert get id');
        $cr = new ConnectionResolver(['default' => $ci]);
        $cr->setDefaultConnection('default');
        $resolver = m::mock($cr);
        $resolver->shouldReceive('getPdo')->andReturn(null);
        return $resolver;
    }
}
