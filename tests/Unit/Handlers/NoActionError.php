<?php

namespace Smorken\Squeue\Tests\Unit\Handlers;

use Smorken\Squeue\Contracts\Results;
use Smorken\Squeue\Handlers\NoAction;

class NoActionError extends NoAction
{

    /**
     * @param $handler_id
     * @param  array  $options
     * @return Results
     */
    public function handle(string|int $handler_id, array $options = []): Results
    {
        $this->setOptions($options);
        $this->handleOptions();
        $models = $this->getModels($handler_id);
        $this->updateAll($models, true);
        return $this->getResults();
    }
}
