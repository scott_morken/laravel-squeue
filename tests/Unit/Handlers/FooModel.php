<?php


namespace Smorken\Squeue\Tests\Unit\Handlers;

use Mockery as m;

class FooModel
{

    function find($id)
    {
        $m = new FooModel();
        $m->id = $id;
        return m::mock($m);
    }
}
