<?php


namespace Smorken\Squeue\Tests\Traits;


trait EnsureDatabase
{

    protected function ensureTestingDatabase($file = null)
    {
        if (is_null($file)) {
            $file = env('DB_DATABASE', '/app/tests/database/testing.sqlite');
        }
        if ($file !== ':memory:' && !file_exists($file)) {
            touch($file);
        }
    }
}
