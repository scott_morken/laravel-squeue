<?php


namespace Smorken\Squeue\Tests\Traits;


use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

trait Table
{

    protected function createTableFromAttributes(
        string $table,
        array $attributes,
        array $additional = [],
        $connection = null
    ): void {
        $s = Schema::connection($connection);
        if (!$s->hasTable($table)) {
            $s->create($table, function (Blueprint $t) use ($attributes, $additional) {
                foreach ($attributes as $k) {
                    $t->string($k)
                      ->nullable();
                }
                $t->timestamps();
                foreach ($additional as $cmd => $data) {
                    call_user_func_array([$t, $cmd], $data ?? []);
                }
            });
        }
    }

    protected function createTableFromModel(
        string $table,
        object $model,
        array $additional = [],
        $connection = null
    ): void {
        $connection = $connection ?? $model->getConnectionName();
        if (is_null($table)) {
            $table = $model->getTable();
        }
        $this->createTableFromAttributes($table, array_keys($model->getAttributes()), $additional, $connection);
    }

    protected function createTableFromModelClass(
        string $table,
        string $model,
        array $additional = [],
        $connection = null
    ): void {
        $m = factory($model)->make();
        $this->createTableFromModel($table, $m, $additional, $connection);
    }

    protected function deleteTable(string $table, $connection = null)
    {
        $s = Schema::connection($connection);
        $s->dropIfExists($table);
    }
}
