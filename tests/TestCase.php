<?php namespace Smorken\Squeue\Tests;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Gate;
use Laravel\BrowserKitTesting\Concerns\ImpersonatesUsers;
use Laravel\BrowserKitTesting\Concerns\InteractsWithAuthentication;
use Laravel\BrowserKitTesting\Concerns\InteractsWithConsole;
use Laravel\BrowserKitTesting\Concerns\InteractsWithContainer;
use Laravel\BrowserKitTesting\Concerns\InteractsWithDatabase;
use Laravel\BrowserKitTesting\Concerns\InteractsWithExceptionHandling;
use Laravel\BrowserKitTesting\Concerns\InteractsWithSession;
use Laravel\BrowserKitTesting\Concerns\MakesHttpRequests;
use Laravel\BrowserKitTesting\Concerns\MocksApplicationServices;
use Orchestra\Testbench\Concerns\Testing;
use Smorken\Service\ServiceProvider;

abstract class TestCase extends \PHPUnit\Framework\TestCase
{

    use ImpersonatesUsers,
        InteractsWithAuthentication,
        InteractsWithConsole,
        InteractsWithContainer,
        InteractsWithDatabase,
        InteractsWithExceptionHandling,
        InteractsWithSession,
        MakesHttpRequests,
        MocksApplicationServices,
        Testing;

    protected string $baseUrl = 'http://localhost';

    protected function authUser(int $id = 1): User
    {
        $first = 'First'.$id;
        $last = 'Last'.$id;
        return (new User())->forceFill(['id' => $id, 'first_name' => $first, 'last_name' => $last]);
    }

    protected function defineGates(Application $app): void
    {
        Gate::define('role-admin', function (User $user) {
            return $user->id === 1;
        });
    }

    /**
     * Define environment setup.
     *
     * @param  Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        // Setup default database to use sqlite :memory:
        $app['config']->set('database.default', 'testbench');
        $app['config']->set(
            'database.connections.testbench',
            [
                'driver' => 'sqlite',
                'database' => ':memory:',
                'prefix' => '',
            ]
        );
        $app['config']->set('squeue.view_master', 'smorken/squeue::layouts.master');
        $app['config']->set('mail.driver', 'array');
    }

    protected function getPackageProviders($app)
    {
        return [
            \Smorken\Squeue\ServiceProvider::class,
            ServiceProvider::class,
            \Smorken\Support\ServiceProvider::class,
        ];
    }

    /**
     * Refresh the application instance.
     *
     * @return void
     */
    protected function refreshApplication()
    {
        $_ENV['APP_ENV'] = 'testing';

        $this->app = $this->createApplication();
    }

    protected function setUp(): void
    {
        $this->setUpTheTestEnvironment();
        $this->defineGates($this->app);
    }

    /**
     * Boot the testing helper traits.
     *
     * @return array<string, string>
     */
    protected function setUpTraits()
    {
        $uses = array_flip(class_uses_recursive(static::class));

        return $this->setUpTheTestEnvironmentTraits($uses);
    }

    protected function tearDown(): void
    {
        $this->tearDownTheTestEnvironment();
    }
}
