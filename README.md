## Laravel Simple Queues

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+
* [Composer](https://getcomposer.org/)

### Composer
"smorken/squeue": "^6.0"

### Use
* Service Provider should be automatically included, if not:
    * Add service provider to `config/app.php`
        * `\Smorken\Squeue\ServiceProvider::class`
* Publish the config/views tags and run the database migrations
    * `php artisan vendor:publish --provider="Smorken\Importer\ServiceProvider"`
    * `php artisan migrate`
* Edit the config file `config/squeue.php` and `config/squeue_handlers.php`
* Create your handlers and providers and add to the config

#### Artisan commands
* `php artisan squeue:process [handler_id]`
