<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/15/14
 * Time: 9:59 AM
 */
return [
    'load_routes' => true,
    'view_master' => env('SQUEUE_VIEW_MASTER', 'layouts.app'),
    'max_age' => '-1 month',
    'max_attempts' => 3,
    'controller' => \Smorken\Squeue\Http\Controllers\SqueueController::class,
    'middleware' => ['web', 'auth', 'can:role-admin'],
    'route_prefix' => 'admin',
    'storage' => [
        'contract' => [
            \Smorken\Squeue\Contracts\Storage\Handler::class => \Smorken\Squeue\Storage\Config\Handler::class,
            \Smorken\Squeue\Contracts\Storage\Squeue::class => \Smorken\Squeue\Storage\Eloquent\Squeue::class,
        ],
        'concrete' => [
            \Smorken\Squeue\Storage\Config\Handler::class => [
                'model' => [
                    'impl' => \Smorken\Squeue\Models\Config\Handler::class,
                ],
            ],
            \Smorken\Squeue\Storage\Eloquent\Squeue::class => [
                'model' => [
                    'impl' => \Smorken\Squeue\Models\Eloquent\Squeue::class,
                ],
            ],
        ],
    ],
];
