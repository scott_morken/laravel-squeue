<?php
return [
    [
        'id' => 'squeue_noaction',
        'interface' => \Smorken\Squeue\Contracts\Handlers\NoAction::class,
        'handler' => \Smorken\Squeue\Handlers\NoAction::class,
        'providers' => [], //array of class names that can be built by the DI container
        'options' => [],
    ],
    [
        'id' => 'squeue_emailer',
        'interface' => \Smorken\Squeue\Contracts\Handlers\Emailer::class,
        'handler' => \Smorken\Squeue\Handlers\Emailer::class,
        'providers' => [],
        'options' => [
            'from' => env('MAIL_FROM_ADDRESS', 'email@example.com'),
        ],
    ],
];
