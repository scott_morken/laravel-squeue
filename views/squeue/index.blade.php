@extends(\Illuminate\Support\Facades\Config::get('squeue.view_master', 'layouts.app'))
@section('content')
    <h1>Simple Queue Administration</h1>
    @include('smorken/squeue::squeue._filter_form')
    @if ($models && count($models))
        <table class="table table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>Identity</th>
                <th>Handler ID</th>
                <th>Result</th>
                <th>Attempts</th>
                <th>Completed</th>
                <th>Last Update</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($models as $model)
                <tr>
                    <td>
                        <a href="{{ action([$controller, 'view'], array_merge($filter->toArray(), ['id' => $model->id])) }}"
                           title="View {{ $model->id }}">{{ $model->id }}</a>
                    </td>
                    <td>{{ $model->identity }}</td>
                    <td>
                        <?php $p_params = array_merge($filter->toArray(), ['handlerId' => $model->handler_id]); ?>
                        <a href="{{ action([$controller, 'process'], $p_params) }}"
                           title="Process {{ $model->handler_id }}">
                            {{ $model->handler_id }}
                        </a>
                    </td>
                    <td>{{ $model->result ?? '--' }}</td>
                    <td>{{ $model->attempts }}</td>
                    <td>{{ $model->completed_at ?? '--' }}</td>
                    <td>{{ $model->updated_at }}</td>
                    <td>
                        <a href="{{ action([$controller, 'delete'], array_merge($filter->toArray(), ['id' => $model->id])) }}"
                           title="Delete {{ $model->id }}" class="text-danger">delete</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if (method_exists($models, 'links'))
            {{ $models->appends($filter->except(['page']))->links() }}
        @endif
    @else
        No records found.
    @endif
@endsection
