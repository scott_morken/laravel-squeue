<?php $filtered = 'border border-success'; ?>
<div class="card mt-2 mb-2">
    <div class="card-body">
        <form class="form-inline" method="get">
            <div class="form-group mb-2 mr-2">
                <label class="sr-only visually-hidden" for="f_identity">Identity</label>
                <input type="text" id="identity" name="f_identity" value="{{ $filter->f_identity }}"
                       class="form-control {{ $filter->f_identity ? $filtered : '' }}" maxlength="64"
                       placeholder="3... (ID)">
            </div>
            <div class="form-group mb-2 mr-2">
                <label class="sr-only visually-hidden" for="f_handlerId">Handler</label>
                <input type="text" id="f_handlerId" name="f_handlerId" value="{{ $filter->f_handlerId }}"
                       class="form-control {{ $filter->f_handlerId ? $filtered : '' }}" maxlength="64"
                       placeholder="handler id">
            </div>
            <div class="form-group mb-2 mr-2">
                <label class="sr-only visually-hidden" for="f_result">Result</label>
                <?php $items = [
                    '' => 'Any Result',
                    \Smorken\Squeue\Contracts\Models\Squeue::COMPLETE => 'Complete',
                    \Smorken\Squeue\Contracts\Models\Squeue::ERROR => 'Error',
                    \Smorken\Squeue\Contracts\Models\Squeue::PENDING => 'Pending'
                ];
                ?>
                <select name="f_result" id="f_result" class="form-control {{ $filter->result ? $filtered : '' }}">
                    @foreach ($items as $k => $v)
                        <option value="{{ $k }}" {{ $filter->result && $filter->result == $k ? 'selected' : ''}}>{{ $v }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group mb-2 mr-2">
                <label class="sr-only visually-hidden" for="f_completedOn">Completed On</label>
                <input type="date" id="f_completedOn" name="f_completedOn" value="{{ $filter->f_completedOn }}"
                       class="form-control {{ $filter->f_completedOn ? $filtered : '' }}" maxlength="64">
            </div>
            <button type="submit" class="btn btn-primary mb-2 mr-2">Filter</button>
            <a href="{{ action([$controller, 'index']) }}" class="btn btn-outline-danger mb-2" title="Reset filter">Reset</a>
        </form>
    </div>
</div>
