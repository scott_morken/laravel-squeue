<?php
$first_col = 'col-sm-2';
$second_col = 'col-sm-10';
$filter_params = $filter->toArray();
?>
@extends(\Illuminate\Support\Facades\Config::get('squeue.view_master', 'layouts.app'))
@section('content')
    <div class="mb-2 float-end">
        <a href="{{ action([$controller, 'index'], $filter_params) }}" title="Back to list">Back</a>
    </div>
    <h1>Simple Queue Administration</h1>
    <h5>View record [{{ $model->id }}]</h5>
    <div class="row mb-4">
        <div class="col">
            <a href="{{ action([$controller, 'delete'], array_merge($filter_params, ['id' => $model->getKey()])) }}"
               title="Delete {{ $model->getKey() }}"
               class="btn btn-danger btn-block">Delete</a>
        </div>
    </div>
    <div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">ID</div>
            <div class="{{ $second_col }}">{{ $model->id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Handler ID</div>
            <div class="{{ $second_col }}">{{ $model->handler_id }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Handler</div>
            <div class="{{ $second_col }}">{{ $model->handler ? $model->handler->handler : 'Invalid' }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Identity</div>
            <div class="{{ $second_col }}">{{ $model->identity }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Result</div>
            <div class="{{ $second_col }}">{{ $model->result }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Attempts</div>
            <div class="{{ $second_col }}">{{ $model->attempts }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Completed</div>
            <div class="{{ $second_col }}">{{ $model->completed_at }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Created</div>
            <div class="{{ $second_col }}">{{ $model->created_at }}</div>
        </div>
        <div class="row mb-2">
            <div class="{{ $first_col }} font-weight-bold">Updated</div>
            <div class="{{ $second_col }}">{{ $model->updated_at }}</div>
        </div>
    </div>
    <div>
        <pre>{{ \Smorken\Support\Arr::stringify($model->data ?: []) }}</pre>
    </div>
    <div>
        {!! $view !!}
    </div>
@endsection
