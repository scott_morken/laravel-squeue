<div>
    Queue lookup results: {{ count($lookups) }}
    @if ($lookups && count($lookups))
        @foreach($lookups as $group_collection)
            @foreach($group_collection as $p => $m)
                <div><b>{{ $p }}:</b> {{ (string)($m) }}</div>
            @endforeach
        @endforeach
    @endif
</div>
