<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 11:03 AM
 */

namespace Smorken\Squeue\Contracts;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Smorken\Squeue\Contracts\Handlers\Base;
use Smorken\Squeue\Contracts\Models\Squeue;
use Smorken\Squeue\Contracts\Storage\Handler;

interface Processor
{

    /**
     * @return Application
     */
    public function getApp(): Application;

    /**
     * @param  int|string  $handler_id
     * @return \Smorken\Squeue\Contracts\Handlers\Base|null
     */
    public function getHandlerById(int|string $handler_id): ?Base;

    /**
     * @return Handler
     */
    public function getHandlerProvider(): Handler;

    public function process(int|string $handler_id, array $options = []): Results;

    public function processAll(array $options = []): Results;

    public function view(Squeue $model): View;
}
