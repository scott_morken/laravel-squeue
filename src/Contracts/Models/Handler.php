<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 6:59 AM
 */

namespace Smorken\Squeue\Contracts\Models;

use Smorken\Model\Contracts\Model;

/**
 * Interface HandlerProvider
 *
 * @package Smorken\Squeue\Contracts\Models
 *
 * @property string $id
 * @property string $interface
 * @property string $handler
 */
interface Handler extends Model
{

}
