<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/20/16
 * Time: 2:56 PM
 */

namespace Smorken\Squeue\Contracts\Models;

use Carbon\Carbon;
use Smorken\Model\Contracts\Model;

/**
 * Interface Squeue
 *
 * @package Smorken\Squeue\Contracts\Models
 *
 * @property int $id
 * @property string $handler_id
 * @property string $identity
 * @property mixed $data
 * @property string $result
 * @property int $attempts
 * @property Carbon|null $completed_at
 *
 * @property Handler $handler
 */
interface Squeue extends Model
{

    const COMPLETE = 'C';
    const ERROR = 'E';
    const PENDING = 'P';
}
