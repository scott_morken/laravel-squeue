<?php

namespace Smorken\Squeue\Contracts;

use JetBrains\PhpStorm\ExpectedValues;

interface Results extends \Stringable
{

    const COMPLETE_ERROR = 'complete_error';
    const COMPLETE_OK = 'complete_ok';
    const ERROR = 'error';

    public function get(#[ExpectedValues(valuesFromClass: Results::class)] string $counter): int;

    public function increment(
        #[ExpectedValues(valuesFromClass: Results::class)] string $counter,
        int $increment = 1
    ): self;

    public function merge(Results $results): void;

    public function toArray(): array;
}
