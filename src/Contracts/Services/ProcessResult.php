<?php

namespace Smorken\Squeue\Contracts\Services;

use Smorken\Service\Contracts\Services\VO\VOResult;

/**
 * @property \Smorken\Squeue\Contracts\Results $results
 */
interface ProcessResult extends VOResult
{

}
