<?php

namespace Smorken\Squeue\Contracts\Services;

use Smorken\Service\Contracts\Services\HasFilterService;

interface IndexService extends \Smorken\Service\Contracts\Services\IndexService, HasFilterService
{

}
