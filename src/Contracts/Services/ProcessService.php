<?php

namespace Smorken\Squeue\Contracts\Services;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Service\Contracts\Services\BaseService;
use Smorken\Squeue\Contracts\Models\Squeue;
use Smorken\Squeue\Contracts\Processor;

interface ProcessService extends BaseService
{

    public function getProcessor(): Processor;

    public function getView(Squeue $squeue): View;

    public function process(Request $request, string|int $handlerId, array $options = []): ProcessResult;

    public function processAll(array $options = []): ProcessResult;

    public function processHandlerId(string|int $handlerId, array $options = []): ProcessResult;
}
