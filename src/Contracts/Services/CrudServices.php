<?php

namespace Smorken\Squeue\Contracts\Services;

interface CrudServices extends \Smorken\Service\Contracts\Services\CrudServices
{

    public function getProcessService(): ProcessService;
}
