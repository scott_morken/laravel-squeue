<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 11:02 AM
 */

namespace Smorken\Squeue\Contracts\Storage;

use Illuminate\Support\Collection;
use Smorken\Storage\Contracts\Storage\Base;

interface Squeue extends Base
{

    /**
     * @param  string  $age
     * @param  bool  $dry_run
     * @return int
     */
    public function clean(string $age = '-7 days', bool $dry_run = false): int;

    /**
     * @param $handler
     * @return Collection<\Smorken\Squeue\Contracts\Models\Squeue>
     */
    public function getPendingByHandler(int|string $handler): Collection;

    /**
     * @param $handler_id
     * @param $identity
     * @param  array|null  $data
     * @return \Smorken\Squeue\Contracts\Models\Squeue|null
     */
    public function queue(
        int|string $handler_id,
        int|string $identity,
        ?array $data
    ): ?\Smorken\Squeue\Contracts\Models\Squeue;
}
