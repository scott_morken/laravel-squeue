<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 10:34 AM
 */

namespace Smorken\Squeue\Contracts\Handlers;

use Illuminate\Contracts\Mail\Mailer;

interface Emailer extends Base
{

    /**
     * @return Mailer
     */
    public function getMailer(): Mailer;

    /**
     * @return string
     */
    public function getViewName(): string;

    /**
     * @param $view
     * @return void
     */
    public function setViewName(string $view): void;
}
