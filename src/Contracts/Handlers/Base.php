<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 9:41 AM
 */

namespace Smorken\Squeue\Contracts\Handlers;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Smorken\Squeue\Contracts\Results;
use Smorken\Squeue\Contracts\Storage\Squeue;

interface Base
{

    /**
     * @return Application
     */
    public function getApp(): Application;

    /**
     * @return ExceptionHandler
     */
    public function getExceptionHandler(): ExceptionHandler;

    /**
     * @return int
     */
    public function getMaxAttempts(): int;

    public function getOption(string $name, mixed $default = null): mixed;

    /**
     * @param  string  $name
     * @return mixed|null
     */
    public function getProvider(string $name): mixed;

    /**
     * @return Results
     */
    public function getResults(): Results;

    /**
     * @return Squeue
     */
    public function getSqueueProvider(): Squeue;

    /**
     * @param $handler_id
     * @param  array  $options
     * @return Results
     */
    public function handle(string|int $handler_id, array $options = []): Results;

    /**
     * @param  int  $attempts
     * @return void
     */
    public function setMaxAttempts(int $attempts): void;

    public function setOption(string $name, mixed $value): void;

    public function setOptions(array $options = []): void;

    /**
     * @param  string|int  $name
     * @param  object|string|null  $obj
     * @return mixed
     */
    public function setProvider(string|int $name, object|string|null $obj = null): void;

    /**
     * @param  array  $providers
     */
    public function setProviders(array $providers): void;

    public function view(\Smorken\Squeue\Contracts\Models\Squeue $model): View;
}
