<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 11:07 AM
 */

namespace Smorken\Squeue;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\View;
use Smorken\Squeue\Contracts\Handlers\Base;
use Smorken\Squeue\Contracts\Models\Squeue;
use Smorken\Squeue\Contracts\Storage\Handler;

class Processor implements Contracts\Processor
{

    protected Application $app;

    protected Handler $handlerProvider;

    protected \Smorken\Squeue\Contracts\Results $results;

    public function __construct(Application $app, Handler $handler)
    {
        $this->app = $app;
        $this->handlerProvider = $handler;
        $this->results = new Results();
    }

    /**
     * @return Application
     */
    public function getApp(): Application
    {
        return $this->app;
    }

    /**
     * @param  string|int  $handler_id
     * @return Base|null
     */
    public function getHandlerById(string|int $handler_id): ?Base
    {
        $model = $this->getHandlerProvider()->find($handler_id);
        if ($model) {
            return $this->getHandlerFromModel($model);
        }
        return null;
    }

    /**
     * @return Handler
     */
    public function getHandlerProvider(): Handler
    {
        return $this->handlerProvider;
    }

    public function process(string|int $handler_id, array $options = []): \Smorken\Squeue\Contracts\Results
    {
        $handler = $this->getHandlerById($handler_id);
        if ($handler) {
            $this->processWithHandler($handler, $handler_id, $options);
        }
        return $this->results;
    }

    public function processAll(array $options = []): \Smorken\Squeue\Contracts\Results
    {
        $handlers = $this->getHandlerProvider()->all();
        foreach ($handlers as $handler_model) {
            $handler = $this->getHandlerFromModel($handler_model);
            if ($handler) {
                $this->processWithHandler($handler, $handler_model->id, $options);
            }
        }
        return $this->results;
    }

    public function view(Squeue $model): View
    {
        $handler = $this->getHandlerFromModel($model->handler);
        return $handler->view($model);
    }

    protected function getHandlerFromModel(Contracts\Models\Handler $model): ?Base
    {
        try {
            return $this->getApp()->make($model->interface);
        } catch (BindingResolutionException $e) {
            // do nothing
        }
        return null;
    }

    protected function processWithHandler(Base $handler, string|int $handler_id, array $options = [])
    {
        $this->results->merge($handler->handle($handler_id, $options));
    }
}
