<?php

namespace Smorken\Squeue;

use JetBrains\PhpStorm\ExpectedValues;

class Results implements \Smorken\Squeue\Contracts\Results
{

    protected array $counters = [
        self::COMPLETE_ERROR => 0,
        self::COMPLETE_OK => 0,
        self::ERROR => 0,
    ];

    public function __toString(): string
    {
        $parts = [];
        foreach ($this->toArray() as $counter => $count) {
            $parts[] = sprintf('%s: %d', $counter, $count);
        }
        return implode(', ', $parts);
    }

    public function get(#[ExpectedValues(valuesFromClass: \Smorken\Squeue\Contracts\Results::class)] string $counter
    ): int {
        return $this->counters[$counter];
    }

    public function increment(
        #[ExpectedValues(valuesFromClass: \Smorken\Squeue\Contracts\Results::class)] string $counter,
        int $increment = 1
    ): self {
        $this->counters[$counter] += $increment;
        return $this;
    }

    public function merge(\Smorken\Squeue\Contracts\Results $results): void
    {
        foreach ($results->toArray() as $counter => $count) {
            $this->increment($counter, $count);
        }
    }

    public function toArray(): array
    {
        return $this->counters;
    }
}
