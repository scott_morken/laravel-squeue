<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 9:41 AM
 */

namespace Smorken\Squeue\Handlers;

use Illuminate\Support\Collection;
use Smorken\Squeue\Contracts\Results;

class NoAction extends Base implements \Smorken\Squeue\Contracts\Handlers\Base
{

    /**
     * @param $handler_id
     * @param  array  $options
     * @return Results
     */
    public function handle(string|int $handler_id, array $options = []): Results
    {
        $this->setOptions($options);
        $this->handleOptions();
        $models = $this->getModels($handler_id);
        $this->updateAll($models);
        return $this->getResults();
    }

    protected function handleCollected(Collection $collected): void
    {
        return;
    }
}
