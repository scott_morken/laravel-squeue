<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 10:36 AM
 */

namespace Smorken\Squeue\Handlers;

use Exception;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Collection;
use Smorken\Squeue\Contracts\Storage\Squeue;
use Smorken\Squeue\SqueueException;

class Emailer extends Base implements \Smorken\Squeue\Contracts\Handlers\Emailer
{

    /**
     * @var Mailer
     */
    protected Mailer $mailer;

    public function __construct(
        Mailer $mailer,
        Application $app,
        ExceptionHandler $exceptionHandler,
        Squeue $squeueProvider
    ) {
        $this->mailer = $mailer;
        parent::__construct($app, $exceptionHandler, $squeueProvider);
    }

    /**
     * @return Mailer
     * @throws SqueueException
     */
    public function getMailer(): Mailer
    {
        return $this->mailer;
    }

    protected function getFrom(): string
    {
        return $this->getOption('from');
    }

    protected function getSubject(Collection $collection): string
    {
        return 'Queue processed notification';
    }

    protected function getTo(Collection $collection): string
    {
        foreach ($collection['lookups'] as $group) {
            foreach ($group as $provider => $model) {
                if ($provider === 'email') {
                    return $model;
                }
                if (stripos($provider, 'user') !== false) {
                    $email = $model->email;
                    if ($email) {
                        return $email;
                    }
                }
            }
        }
        throw new SqueueException('No to email address found.');
    }

    protected function handleCollected(Collection $collected): void
    {
        foreach ($collected as $identity => $sl_collection) {
            try {
                $data = $this->options;
                $data['lookups'] = $sl_collection['lookups'];
                $this->getMailer()->send(
                    $this->getViewName(),
                    $data,
                    function ($message) use ($sl_collection) {
                        $message->to($this->getTo($sl_collection));
                        $message->from($this->getFrom());
                        $message->subject($this->getSubject($sl_collection));
                    }
                );
                $this->updateAll($sl_collection['squeues']);
            } catch (Exception $e) {
                $this->getExceptionHandler()->report($e);
                $this->updateAll($sl_collection['squeues'], true);
            }
        }
    }
}
