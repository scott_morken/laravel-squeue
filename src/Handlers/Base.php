<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 9:42 AM
 */

namespace Smorken\Squeue\Handlers;

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\View;
use JetBrains\PhpStorm\ExpectedValues;
use ReflectionException;
use Smorken\Squeue\Contracts\Results;
use Smorken\Squeue\Contracts\Storage\Squeue;
use Smorken\Squeue\SqueueException;

abstract class Base implements \Smorken\Squeue\Contracts\Handlers\Base
{

    /**
     * @var Application
     */
    protected Application $app;

    /**
     * @var ExceptionHandler
     */
    protected ExceptionHandler $exceptionHandler;

    protected array $lookups = [];

    protected int $max_attempts = 3;

    protected array $options = [];

    protected array $providers = [];

    protected Results $results;

    /**
     * @var Squeue
     */
    protected Squeue $squeueProvider;

    public function __construct(Application $app, ExceptionHandler $exceptionHandler, Squeue $squeueProvider)
    {
        $this->app = $app;
        $this->exceptionHandler = $exceptionHandler;
        $this->squeueProvider = $squeueProvider;
        $this->results = new \Smorken\Squeue\Results();
    }

    abstract protected function handleCollected(Collection $collected);

    /**
     * @return Application
     */
    public function getApp(): Application
    {
        return $this->app;
    }

    /**
     * @return ExceptionHandler
     */
    public function getExceptionHandler(): ExceptionHandler
    {
        return $this->exceptionHandler;
    }

    public function getMaxAttempts(): int
    {
        return $this->max_attempts;
    }

    public function setMaxAttempts(int $attempts): void
    {
        $this->max_attempts = $attempts;
    }

    public function getOption(string $name, mixed $default = null): mixed
    {
        return Arr::get($this->options, $name, $default);
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function getProvider(string $name): mixed
    {
        $p = Arr::get($this->providers, $name);
        if (is_null($p)) {
            try {
                $this->setProvider($name);
            } catch (ReflectionException $e) {
                $this->getExceptionHandler()->report($e);
            }
        }
        return Arr::get($this->providers, $name);
    }

    /**
     * @return Results
     */
    public function getResults(): Results
    {
        return $this->results;
    }

    /**
     * @return Squeue
     */
    public function getSqueueProvider(): Squeue
    {
        return $this->squeueProvider;
    }

    /**
     * @return string
     * @throws SqueueException
     */
    public function getViewName(): string
    {
        return $this->getOption('view_name', 'smorken/squeue::emailer.default');
    }

    /**
     * @param $handler_id
     * @param  array  $options
     * @return array
     */
    public function handle(string|int $handler_id, array $options = []): Results
    {
        $this->setOptions($options);
        $this->handleOptions();
        $models = $this->getModels($handler_id);
        $collection = $this->collectByIdentity($models);
        $this->handleCollected($collection);
        return $this->getResults();
    }

    public function setOption(string $name, mixed $value): void
    {
        Arr::set($this->options, $name, $value);
    }

    public function setOptions(array $options = []): void
    {
        foreach ($options as $k => $v) {
            $this->setOption($k, $v);
        }
    }

    public function setProvider(string|int $name, object|string|null $obj = null): void
    {
        if (is_int($name) && is_string($obj)) {
            $name = $obj;
            $obj = null;
        }
        if (is_null($obj)) {
            try {
                $this->providers[$name] = $this->getApp()->make($name);
            } catch (BindingResolutionException $e) {
                $this->providers[$name] = null;
            }
        } else {
            $this->providers[$name] = $obj;
        }
    }

    /**
     * @param  array  $providers
     */
    public function setProviders(array $providers): void
    {
        foreach ($providers as $name => $obj) {
            $this->setProvider($name, $obj);
        }
    }

    /**
     * @param $view
     * @return void
     */
    public function setViewName(string $view): void
    {
        $this->setOption('view_name', $view);
    }

    public function view(\Smorken\Squeue\Contracts\Models\Squeue $model): \Illuminate\Contracts\View\View
    {
        $coll = $this->collectByIdentity(new Collection([$model]));
        $data = $this->options;
        $data['lookups'] = $coll->first()['lookups'];
        return View::make($this->getViewName(), $data);
    }

    protected function addLookups(\Smorken\Squeue\Contracts\Models\Squeue $model, Collection $collection): void
    {
        $data = $model->data ?: [];
        $group_coll = new Collection();
        $collection->push($group_coll);
        foreach ($data as $provider => $id) {
            if ($provider && $id) {
                $m = $this->lookup($provider, $id);
                $group_coll->put($provider, $m);
            }
        }
    }

    protected function addSqueueModel(
        \Smorken\Squeue\Contracts\Models\Squeue $model,
        Collection $collection
    ): void {
        $collection->put($model->id, $model);
    }

    protected function collectByIdentity(Collection $models): Collection
    {
        $coll = new Collection();
        foreach ($models as $model) {
            $identity = $this->getIdentity($model);
            if (!$coll->has($identity)) {
                $coll->put($identity, new Collection(['squeues' => new Collection(), 'lookups' => new Collection()]));
            }
            $current = $coll->get($identity);
            $this->addSqueueModel($model, $current['squeues']);
            $this->addLookups($model, $current['lookups']);
        }
        return $coll;
    }

    protected function getIdentity($model): int|string
    {
        return $model->identity ?: 0;
    }

    protected function getModels(string|int $handler_id): Collection
    {
        return $this->getSqueueProvider()->getPendingByHandler($handler_id);
    }

    protected function handleOptions(array $options = []): void
    {
        return;
    }

    protected function incrementResult(#[ExpectedValues(valuesFromClass: Results::class)] string $type): void
    {
        $this->getResults()->increment($type);
    }

    protected function lookup(string $provider, string|int $id): mixed
    {
        if (!isset($this->lookups[$provider])) {
            $this->lookups[$provider] = [];
        }
        if (!isset($this->lookups[$provider][$id])) {
            $p = $this->getProvider($provider);
            $m = $id;
            if ($p && is_object($p)) {
                if (method_exists($p, 'find')) {
                    $m = $p->find($id);
                } elseif (method_exists($p, 'getById')) {
                    $m = $p->getById($id);
                }
            }
            $this->lookups[$provider][$id] = $m;
            return $m;
        }
        return $this->lookups[$provider][$id];
    }

    protected function update(\Smorken\Squeue\Contracts\Models\Squeue $model, bool $is_error = false): bool
    {
        $now = date('Y-m-d H:i:s');
        $model->attempts = $model->attempts + 1;
        if ($is_error) {
            $model->result = \Smorken\Squeue\Contracts\Models\Squeue::ERROR;
            if ($model->attempts > $this->getMaxAttempts()) {
                $model->completed_at = $now;
                $this->incrementResult(Results::COMPLETE_ERROR);
            } else {
                $this->incrementResult(Results::ERROR);
            }
        } else {
            $model->result = \Smorken\Squeue\Contracts\Models\Squeue::COMPLETE;
            $model->completed_at = $now;
            $this->incrementResult(Results::COMPLETE_OK);
        }
        return $model->save();
    }

    protected function updateAll(Collection $collection, bool $is_error = false): bool
    {
        $saved = true;
        foreach ($collection as $model) {
            if (!$this->update($model, $is_error)) {
                $saved = false;
            }
        }
        return $saved;
    }
}
