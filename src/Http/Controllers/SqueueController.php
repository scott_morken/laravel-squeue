<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 10/21/16
 * Time: 7:07 AM
 */

namespace Smorken\Squeue\Http\Controllers;

use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Smorken\Service\Controllers\Controller;
use Smorken\Service\Controllers\Traits\DeleteTrait;
use Smorken\Service\Controllers\Traits\IndexTrait;
use Smorken\Service\Services\VO\RedirectActionResult;
use Smorken\Squeue\Contracts\Services\CrudServices;
use Smorken\Squeue\Contracts\Services\IndexService;

class SqueueController extends Controller
{

    use DeleteTrait, IndexTrait;

    protected string $baseView = 'smorken/squeue::squeue';

    public function __construct(
        IndexService $indexService,
        CrudServices $crudServices
    ) {
        $this->indexService = $indexService;
        $this->crudServices = $crudServices;
        parent::__construct();
    }

    public function process(Request $request, string|int $handlerId): RedirectResponse
    {
        $results = $this->crudServices->getProcessService()->process($request, $handlerId);
        $filter = $this->getFilter($request);
        $request->session()->flash('flash:info', 'Processed '.$results->results);
        return (new RedirectActionResult($this->actionArray('index'), $filter->toArray()))->redirect();
    }

    public function view(Request $request, int|string $id): View
    {
        $results = $this->crudServices->getRetrieveService()->findById($id);
        $view = $this->crudServices->getProcessService()->getView($results->model);
        return $this->makeView($this->getViewName('view'))
                    ->with('model', $results->model)
                    ->with('view', $view)
                    ->with('filter', $this->getFilter($request));
    }
}
