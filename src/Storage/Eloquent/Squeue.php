<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:05 PM
 */

namespace Smorken\Squeue\Storage\Eloquent;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class Squeue extends Base implements \Smorken\Squeue\Contracts\Storage\Squeue
{

    /**
     * @param  string  $age
     * @param  bool  $dry_run
     * @return int
     */
    public function clean(string $age = '-7 days', bool $dry_run = false): int
    {
        $q = $this->getModel()
                  ->newQuery()
                  ->completed()
                  ->olderThan(date('Y-m-d 00:00', strtotime($age)));
        if ($dry_run) {
            return $q->count();
        }
        return $q->delete();
    }

    /**
     * @param $handler
     * @return Collection<\Smorken\Squeue\Contracts\Models\Squeue>
     */
    public function getPendingByHandler(string|int $handler): Collection
    {
        return $this->getModel()
                    ->newQuery()
                    ->pending()
                    ->handlerIdIs($handler)
                    ->orderIdentity()
                    ->get();
    }

    /**
     * @param $handler_id
     * @param $identity
     * @param  array|null  $data
     * @return \Smorken\Squeue\Contracts\Models\Squeue|null
     */
    public function queue(
        string|int $handler_id,
        string|int $identity,
        ?array $data
    ): ?\Smorken\Squeue\Contracts\Models\Squeue {
        $m = $this->getModel()->newInstance();
        $m->handler_id = $handler_id;
        $m->identity = $identity;
        $m->data = $data ?: [];
        $m->result = \Smorken\Squeue\Contracts\Models\Squeue::PENDING;
        $m->save();
        return $m;
    }

    protected function filterCompletedOn(Builder $query, $v): Builder
    {
        if (strlen($v)) {
            $query->completedOn($v);
        }
        return $query;
    }

    protected function filterHandlerId(Builder $query, $v): Builder
    {
        if (strlen($v)) {
            $query->handlerIdIs($v);
        }
        return $query;
    }

    protected function filterIdentity(Builder $query, $v): Builder
    {
        if (strlen($v)) {
            $query->identityIs($v);
        }
        return $query;
    }

    protected function filterResult(Builder $query, $v): Builder
    {
        if (strlen($v)) {
            $query->resultIs($v);
        }
        return $query;
    }

    protected function getFilterMethods(): array
    {
        return [
            'f_identity' => 'filterIdentity',
            'f_handlerId' => 'filterHandlerId',
            'f_result' => 'filterResult',
            'f_completedOn' => 'filterCompletedOn',
        ];
    }
}
