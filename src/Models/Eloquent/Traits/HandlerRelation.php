<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:30 PM
 */

namespace Smorken\Squeue\Models\Eloquent\Traits;

use Smorken\Squeue\Contracts\Storage\Handler;

trait HandlerRelation
{

    /**
     * @var \Smorken\Squeue\Contracts\Models\Handler
     */
    protected ?\Smorken\Squeue\Contracts\Models\Handler $handlerModel = null;

    /**
     * @var Handler
     */
    protected ?Handler $handlerProvider = null;

    /**
     * @return \Smorken\Squeue\Contracts\Models\Handler
     */
    public function getHandlerAttribute(): ?\Smorken\Squeue\Contracts\Models\Handler
    {
        if ($this->handler_id) {
            $this->handlerModel = $this->getHandlerProvider()->find($this->handler_id);
        }
        return $this->handlerModel;
    }

    /**
     * @return Handler
     */
    protected function getHandlerProvider(): Handler
    {
        if (!$this->handlerProvider) {
            $this->handlerProvider = $this->getApp()->make(Handler::class);
        }
        return $this->handlerProvider;
    }

    /**
     * @param  Handler  $handlerProvider
     */
    public function setHandlerProvider(Handler $handlerProvider): void
    {
        $this->handlerProvider = $handlerProvider;
    }
}
