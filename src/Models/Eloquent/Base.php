<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/27/15
 * Time: 9:17 AM
 */

namespace Smorken\Squeue\Models\Eloquent;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\App;
use Smorken\Model\Eloquent;

abstract class Base extends Eloquent
{

    use HasFactory;

    /**
     * @var Application|null
     */
    protected ?Application $app = null;

    /**
     * @return Application
     */
    protected function getApp(): Application
    {
        if (!$this->app) {
            $this->app = App::getFacadeRoot();
        }
        return $this->app;
    }
}
