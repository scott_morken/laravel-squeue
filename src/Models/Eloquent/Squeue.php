<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:32 PM
 */

namespace Smorken\Squeue\Models\Eloquent;

use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Smorken\Squeue\Contracts\Storage\Handler;
use Smorken\Squeue\Models\Eloquent\Traits\HandlerRelation;

class Squeue extends Base implements \Smorken\Squeue\Contracts\Models\Squeue
{

    use HandlerRelation;

    protected $casts = ['data' => 'json'];

    protected $dates = ['created_at', 'updated_at', 'completed_at'];

    protected $fillable = ['handler_id', 'identity', 'data', 'result', 'attempts', 'completed_at'];

    public function getResultAttribute(): string
    {
        if (isset($this->attributes['result']) && $this->attributes['result']) {
            return $this->attributes['result'];
        }
        return self::PENDING;
    }

    public function rules(): array
    {
        $provider = $this->getApp()->make(Handler::class);
        $allowed = $provider->all()->pluck('id')->toArray();
        return [
            'handler_id' => 'required|in:'.implode(',', $allowed),
            'data' => 'required',
            'result' => 'in:C,E,P',
            'attempts' => 'integer',
            'completed_at' => 'date',
        ];
    }

    public function scopeCompleted(Builder $q): Builder
    {
        return $q->whereNotNull('completed_at');
    }

    public function scopeCompletedOn(Builder $q, string $date): Builder
    {
        $base = date('Y-m-d', strtotime($date));
        $start = $base.' 00:00';
        $end = $base.' 23:59:59';
        return $q->whereBetween('completed_at', [$start, $end]);
    }

    public function scopeDefaultOrder(Builder $query): Builder
    {
        return $this->scopeOrderUpdatedAt($query);
    }

    public function scopeHandlerIdIs(Builder $q, int|string $handler_id): Builder
    {
        return $q->where('handler_id', '=', $handler_id);
    }

    public function scopeIdentityIs(Builder $q, int|string $identity): Builder
    {
        return $q->where('identity', '=', $identity);
    }

    public function scopeOlderThan(Builder $q, Carbon|\DateTime|string $date): Builder
    {
        return $q->where('created_at', '<', $date);
    }

    public function scopeOrderCompletedAt(Builder $q): Builder
    {
        return $q->orderBy('completed_at');
    }

    public function scopeOrderIdentity(Builder $q): Builder
    {
        return $q->orderBy('identity');
    }

    public function scopeOrderUpdatedAt(Builder $q): Builder
    {
        return $q->orderBy('updated_at', 'desc');
    }

    public function scopePending(Builder $q): Builder
    {
        return $q->whereNull('completed_at');
    }

    public function scopeResultIs(Builder $q, string $result): Builder
    {
        return $q->where('result', '=', $result);
    }

    public function validationRules(array $override = []): array
    {
        return $this->mergeValidationRules($override, $this->rules());
    }
}
