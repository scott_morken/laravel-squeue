<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 5/4/17
 * Time: 9:03 AM
 */

namespace Smorken\Squeue\Models\Config;

use Smorken\Model\Config;

class Handler extends Config implements \Smorken\Squeue\Contracts\Models\Handler
{

    protected ?string $config_key = 'squeue_handlers';
}
