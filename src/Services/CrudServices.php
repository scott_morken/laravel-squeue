<?php

namespace Smorken\Squeue\Services;

use Smorken\Service\Contracts\Services\CreateService;
use Smorken\Service\Contracts\Services\DeleteService;
use Smorken\Service\Contracts\Services\RetrieveService;
use Smorken\Service\Contracts\Services\UpdateService;
use Smorken\Service\Services\CrudByStorageProviderServices;
use Smorken\Squeue\Contracts\Services\ProcessService;

class CrudServices extends CrudByStorageProviderServices implements
    \Smorken\Squeue\Contracts\Services\CrudServices
{

    protected array $services = [
        CreateService::class => null,
        DeleteService::class => null,
        RetrieveService::class => null,
        UpdateService::class => null,
        ProcessService::class => null,
    ];

    public function getProcessService(): ProcessService
    {
        return $this->getService(ProcessService::class);
    }
}
