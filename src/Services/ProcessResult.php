<?php

namespace Smorken\Squeue\Services;

use Smorken\Service\Services\VO\VOResult;
use Smorken\Squeue\Contracts\Results;

class ProcessResult extends VOResult implements \Smorken\Squeue\Contracts\Services\ProcessResult
{

    public function __construct(public Results $results)
    {
    }
}
