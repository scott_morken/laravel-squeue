<?php

namespace Smorken\Squeue\Services;

use Illuminate\Http\Request;
use Smorken\Support\Contracts\Filter;

class FilterService extends \Smorken\Service\Services\FilterService
{

    protected function createFilterFromRequest(Request $request): Filter
    {
        return new \Smorken\Support\Filter([
            'f_identity' => $request->input('f_identity'),
            'f_handlerId' => $request->input('f_handlerId'),
            'f_result' => $request->input('f_result'),
            'f_completedOn' => $request->input('f_completedOn'),
        ]);
    }
}
