<?php

namespace Smorken\Squeue\Services;

use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Smorken\Service\Services\BaseService;
use Smorken\Squeue\Contracts\Models\Squeue;
use Smorken\Squeue\Contracts\Processor;
use Smorken\Squeue\Contracts\Services\ProcessResult;

class ProcessService extends BaseService implements \Smorken\Squeue\Contracts\Services\ProcessService
{

    protected string $voClass = \Smorken\Squeue\Services\ProcessResult::class;

    public function __construct(protected Processor $processor, array $services = [])
    {
        parent::__construct($services);
    }

    public function getProcessor(): Processor
    {
        return $this->processor;
    }

    public function getView(Squeue $squeue): View
    {
        return $this->getProcessor()->view($squeue);
    }

    public function process(Request $request, int|string $handlerId, array $options = []): ProcessResult
    {
        return $this->processHandlerId($handlerId, $options);
    }

    public function processAll(array $options = []): ProcessResult
    {
        $results = $this->getProcessor()->processAll($options);
        return $this->newVO(['results' => $results]);
    }

    public function processHandlerId(int|string $handlerId, array $options = []): ProcessResult
    {
        $results = $this->getProcessor()->process($handlerId, $options);
        return $this->newVO(['results' => $results]);
    }
}
