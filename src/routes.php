<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/15/14
 * Time: 7:57 AM
 */

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use Smorken\Squeue\Http\Controllers\SqueueController;

Route::middleware(Config::get('squeue.middleware', ['web', 'auth', 'can:role-admin']))
     ->prefix(Config::get('squeue.route_prefix', 'admin'))
     ->group(function () {
         Route::prefix('squeue')
              ->group(function () {
                  $controller = Config::get('squeue.controller',
                      SqueueController::class);
                  Route::get('', [$controller, 'index']);
                  Route::get('delete/{id}', [$controller, 'delete']);
                  Route::delete('delete/{id}', [$controller, 'doDelete']);
                  Route::get('process/{handlerId}', [$controller, 'process']);
                  Route::get('view/{id}', [$controller, 'view']);
              });
     });
