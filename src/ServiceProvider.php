<?php namespace Smorken\Squeue;

use Smorken\Service\Contracts\Services\FilterService;
use Smorken\Squeue\Contracts\Services\CrudServices;
use Smorken\Squeue\Contracts\Services\IndexService;
use Smorken\Squeue\Contracts\Services\ProcessService;
use Smorken\Squeue\Contracts\Storage\Handler;
use Smorken\Squeue\Contracts\Storage\Squeue;
use Smorken\Support\Contracts\Binder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootView();
        $this->bootConfig();
        $this->bootHandlersConfig();
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->bindHandlers();
        $this->loadRoutes();
        $this->commands(
            [
                Console\Commands\Processor::class,
                Console\Commands\Cleanup::class,
            ]
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->bindProcessor();
        $this->bindProviders();
        $this->bindServices();
    }

    protected function bindHandlers()
    {
        $provider = $this->app[Handler::class];
        $handlers = $provider->all();
        $max_attempts = $this->app['config']->get('squeue.max_attempts', 3);
        foreach ($handlers as $handler) {
            $this->app->bind(
                $handler->interface,
                function ($app) use ($handler, $max_attempts) {
                    $h = $app[$handler->handler];
                    $h->setProviders($handler->providers);
                    $h->setOptions($handler->options);
                    $h->setMaxAttempts($max_attempts);
                    return $h;
                }
            );
            $this->app->alias($handler->interface, $handler->id);
        }
    }

    protected function bindProcessor()
    {
        $this->app->bind(
            \Smorken\Squeue\Contracts\Processor::class,
            function ($app) {
                $h = $app[Handler::class];
                return new Processor($app, $h);
            }
        );
    }

    protected function bindProviders(): void
    {
        $contracts = [
            Handler::class,
            Squeue::class,
        ];
        foreach ($contracts as $contract) {
            $this->app->bind($contract, function ($app) use ($contract) {
                $binder = $app[Binder::class];
                $providers = $app['config']->get('squeue.storage', []);
                $concreteClass = $providers['contract'][$contract];
                return $binder->fromIterable($providers['concrete'] ?? [], $concreteClass);
            });
        }
    }

    protected function bindServices(): void
    {
        $this->app->bind(IndexService::class, function ($app) {
            return new \Smorken\Squeue\Services\IndexService($app[Squeue::class], [
                FilterService::class => new \Smorken\Squeue\Services\FilterService(),
            ]);
        });
        $this->app->bind(ProcessService::class, function ($app) {
            return new \Smorken\Squeue\Services\ProcessService($app[\Smorken\Squeue\Contracts\Processor::class]);
        });
        $this->app->bind(CrudServices::class, function ($app) {
            $additionalServices = [
                FilterService::class => new \Smorken\Squeue\Services\FilterService(),
            ];
            $crudServices = \Smorken\Squeue\Services\CrudServices::createByStorageProvider($app[Squeue::class],
                $additionalServices);
            $crudServices->setService(ProcessService::class, $app[ProcessService::class]);
            return $crudServices;
        });
    }

    protected function bootConfig()
    {
        $package_path = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($package_path, 'squeue');
        $this->publishes([$package_path => config_path('squeue.php')], 'config');
    }

    protected function bootHandlersConfig()
    {
        $package_path = __DIR__.'/../config/handlers.php';
        $this->mergeConfigFrom($package_path, 'squeue_handlers');
        $this->publishes([$package_path => config_path('squeue_handlers.php')], 'config');
    }

    protected function bootView()
    {
        $this->loadViewsFrom(__DIR__.'/../views', 'smorken/squeue');
        $this->publishes(
            [
                __DIR__.'/../views' => resource_path('views/vendor/smorken/squeue'),
            ],
            'views'
        );
    }

    protected function loadRoutes()
    {
        if ($this->app['config']->get('squeue.load_routes', true)) {
            $this->loadRoutesFrom(__DIR__.'/routes.php');
        }
    }
}
