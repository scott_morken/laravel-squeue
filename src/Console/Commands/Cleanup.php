<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:26 PM
 */

namespace Smorken\Squeue\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Smorken\Squeue\Contracts\Storage\Squeue;

class Cleanup extends Command
{

    use TimerTrait;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cleanup old queue items that have been completed.';

    /**
     * @var Squeue
     */
    protected Squeue $provider;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'squeue:cleanup {--dry}';

    public function __construct(Squeue $provider)
    {
        $this->provider = $provider;
        parent::__construct();
    }

    public function handle(): int
    {
        $this->outputStart();
        $max_age = Config::get('smorken/squeue::config.max_age', '-1 month');
        $dry = $this->option('dry');
        if ($dry) {
            $this->comment(' >> DRY RUN << ');
        }
        $count = $this->provider->clean($max_age, $dry);
        $this->info(sprintf('Processed %d records.', $count));
        $this->outputFinished();
        return 0;
    }
}
