<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:26 PM
 */

namespace Smorken\Squeue\Console\Commands;

use Carbon\Carbon;

trait TimerTrait
{

    /**
     * @var Carbon
     */
    protected $start;

    protected function getStart()
    {
        if (!$this->start) {
            $this->start = Carbon::now();
        }
        return $this->start->toDateTimeString();
    }

    protected function outputFinished()
    {
        $finish = Carbon::now();
        $this->line('Finished at: '.$finish);
        $this->line('Process began: '.$finish->diffForHumans($this->start));
    }

    protected function outputStart()
    {
        $this->line('Started process at: '.$this->getStart());
    }
}
