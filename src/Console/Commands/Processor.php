<?php
/**
 * Created by IntelliJ IDEA.
 * User: scoce95461
 * Date: 3/24/17
 * Time: 12:26 PM
 */

namespace Smorken\Squeue\Console\Commands;

use Illuminate\Console\Command;
use Smorken\Squeue\Contracts\Results;
use Smorken\Squeue\Contracts\Services\ProcessService;
use Smorken\Support\Arr;

class Processor extends Command
{

    use TimerTrait;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processes the pending queue items.';

    /**
     * @var \Smorken\Squeue\Contracts\Processor
     */
    protected \Smorken\Squeue\Contracts\Processor $processor;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'squeue:process {handler_id? : The handler id}';

    public function __construct(protected ProcessService $processService)
    {
        parent::__construct();
    }

    public function handle(): int
    {
        $this->outputStart();
        $handler_id = $this->argument('handler_id');
        $options = $this->option();
        if ($handler_id) {
            $this->outputResults($this->processService->processHandlerId($handler_id, $options)->results);
        } else {
            $this->outputResults($this->processService->processAll($options)->results);
        }
        $this->outputFinished();
        return 0;
    }

    protected function outputResults(Results $results): void
    {
        $this->line(Arr::stringify($results->toArray()));
    }
}
