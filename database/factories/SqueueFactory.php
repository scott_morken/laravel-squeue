<?php

namespace Database\Factories\Smorken\Squeue\Models\Eloquent;

use Illuminate\Database\Eloquent\Factories\Factory;
use Smorken\Squeue\Models\Eloquent\Squeue;

class SqueueFactory extends Factory
{

    protected $model = Squeue::class;

    public function definition(): array
    {
        return [
            'handler_id' => 'squeue_noaction',
            'identity' => 0,
            'data' => '',
            'result' => '',
            'attempts' => 0,
            'completed_at' => null,
        ];
    }
}
