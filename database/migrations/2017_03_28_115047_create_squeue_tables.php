<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {

    public function down()
    {
        Schema::dropIfExists('squeues');
    }

    public function up()
    {
        Schema::create(
            'squeues',
            function (Blueprint $t) {
                $t->increments('id')->unsigned();
                $t->string('handler_id', 64);
                $t->string('identity', 16)->default(0);
                $t->text('data');
                $t->string('result', 1)->default('');
                $t->tinyInteger('attempts')->unsigned()->default(0);
                $t->dateTime('completed_at')->nullable();
                $t->timestamps();

                $t->index('identity', 'sq_identity_ndx');
                $t->index('completed_at', 'sq_completed_at_ndx');
            }
        );
    }
};
